const app = new Vue({
    el:'#app',
    data:{
        titulo:'PROXIMOS EVENTOS',
        eventos: [],
        nuevoEvento:{
            tema: 'Habilidades Blandas',
            expositor: 'Karla',
            fecha: '2021-03-21',
            url: 'https://facebook.com/CCAT',
            estado: '',
            ubicacion_img:''
        },
        eventosAgrupados: [],
        indice: 0,
        scroll_estado: false,
        cordenada_Y: 0
    },
    methods:{
        ponerNegrita(index){
            for(elemt_evento of this.eventosAgrupados) {
                if (index === this.eventosAgrupados.indexOf(elemt_evento)){
                    // poner en negrita
                    let elemt_a = document.getElementById(index + '-a');
                    elemt_a.style.fontWeight = "bold";
                } else {
                    // sin negrita
                    let elemt_a = document.getElementById(this.eventosAgrupados.indexOf(elemt_evento) + '-a');
                    elemt_a.style.fontWeight = "normal";
                }
            }
        },
        ubicarIndice(index){
            this.indice = index;
            this.ponerNegrita(index)
            this.scroll_estado = true;
            this.cordenada_Y = window.screenY;
        },
        fLeft(){
            if(this.indice - 1 >= 0){
                this.indice = this.indice - 1;
                this.ponerNegrita(this.indice)
            };
            this.scroll_estado = true;
            this.cordenada_Y = window.screenY;
        },
        fRight(){
            if(this.indice + 1 <= this.eventosAgrupados.length - 1){
                this.indice = this.indice + 1;
                this.ponerNegrita(this.indice)
            };
            this.scroll_estado = true;
            this.cordenada_Y = window.screenY;
        },
        agruparEventos(){
            let cont = 0;
            let act = 0;
            let grupoEvento = [];
            this.eventosAgrupados = [];
            for(evento of this.eventos){
                cont++;
                act++;
                if(cont < 5){
                    grupoEvento.push(evento);
                    if(grupoEvento.length === 4){
                        this.eventosAgrupados.push(grupoEvento);
                        grupoEvento = [];
                        cont = 0;
                    }else if(act === this.eventos.length){
                        this.eventosAgrupados.push(grupoEvento);
                        grupoEvento = [];
                    }
                };
                
            }
        }
    },
    created: function(){
        let datosDB = JSON.parse(localStorage.getItem('prox-eventos'));
        if(datosDB === null){
            this.eventos = [];
        }else{
            this.eventos = datosDB;
            this.agruparEventos();
        }
    },
    mounted(){
        //Se ejecuta al insertar el DOM
        let elemt_a = document.getElementById('0-a');
        elemt_a.style.fontWeight = "bold";
    },
})
history.pushState(null,null,"#0")

// volver a su posicion inicial al cargar la pagina
let scroll_inicio;
scroll_inicio = setInterval(() => {
    window.scrollTo(0,0);
}, 10);
setTimeout(() => {
    clearInterval(scroll_inicio);
}, 1000);

// volver a su posicion inicial al presionar los <a>
let scroll_go;
setInterval(() => {
    if(app.scroll_estado){
        scroll_go = setInterval(() => {
            window.scrollTo(0,0);
        }, 10);
        setTimeout(() => {
            clearInterval(scroll_go);
        }, 50);
        app.scroll_estado = false;
        console.log(app.cordenada_Y)
    }
}, 100);
