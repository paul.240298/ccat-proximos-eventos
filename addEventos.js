const app = new Vue({
    el:'#app',
    data:{
        titulo:'PROXIMOS EVENTOS',
        eventos: [],
        nuevoEvento:{
            tema: 'Habilidades Blandas',
            expositor: 'Karla',
            fecha: '2021-03-21',
            url: 'https://facebook.com/CCAT',
            estado: '',
            ubicacion_img:''
        },
        btn_estado: false,
    },
    methods:{
        btnCambiarEstado(){
            let divAddEvento = document.getElementById('addEvent2');
            this.btn_estado = !this.btn_estado;
            console.log(this.btn_estado);
            if(this.btn_estado){
                divAddEvento.style.display = "flex";
            } else {
                divAddEvento.style.display = "none";
            }
        },
        btnClose(){
            let divAddEvento = document.getElementById('addEvent2');
            this.btn_estado = !this.btn_estado;
            console.log(this.btn_estado);
            divAddEvento.style.display = "none";
        },
        agregarEvento(){
            this.eventos.push({
                tema: this.nuevoEvento.tema,
                expositor: this.nuevoEvento.expositor,
                fecha: this.nuevoEvento.fecha,
                url: this.nuevoEvento.url,
                estado: false,
                ubicacion_img: this.nuevoEvento.ubicacion_img
            });
            this.nuevoEvento={
                tema: 'Habilidades Blandas',
                expositor: 'Karla',
                fecha: '2021-03-21',
                url: 'https://facebook.com/CCAT',
                estado: '',
                ubicacion_img:''
            };
            localStorage.setItem('prox-eventos',JSON.stringify(this.eventos));
        },
        cambiar(index){
            this.eventos[index].estado=!this.eventos[index].estado;
            localStorage.setItem('prox-eventos',JSON.stringify(this.eventos));
        },
        eliminar(index){
            this.eventos.splice(index,1);
            localStorage.setItem('prox-eventos',JSON.stringify(this.eventos));
        }
    },
    created: function(){
        let datosDB = JSON.parse(localStorage.getItem('prox-eventos'));
        if(datosDB === null){
            this.eventos = [];
        }else{
            this.eventos = datosDB;
        }
    }
})
try {
    document.getElementById('uploadImage').onchange = function(e){
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload= function(){
            let preview = document.getElementById('preview-img');
            image = document.createElement('img');
            image.src = reader.result;
            preview.innerHTML = '';
            preview.append(image);
        }
    }
} catch (error) {
    console.log(error);
}